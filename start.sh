#!/bin/sh

sed -i "s/MYSQL_DATABASE/$MYSQL_DATABASE/g" /docker-entrypoint-initdb.d/initdb.sql
sed -i "s/MYSQL_USER/$MYSQL_USER/g" /docker-entrypoint-initdb.d/initdb.sql
sed -i "s/MYSQL_PASSWORD/$MYSQL_PASSWORD/g" /docker-entrypoint-initdb.d/initdb.sql

mysql_install_db --user=mysql --datadir=/var/lib/mysql

mysqld_safe --user=mysql &
sleep 5s

mysql -u root -e "CREATE USER 'root'@'%' IDENTIFIED BY '$MYSQL_ROOT_PASSWORD';"
mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;"
mysql -u root -e "FLUSH PRIVILEGES;"

mysql -u root < /docker-entrypoint-initdb.d/initdb.sql

tail -f /dev/null
