FROM alpine:latest

RUN apk update && \
    apk add --no-cache mysql mysql-client python3 && \
    apk add --no-cache py-pip mariadb-dev gcc musl-dev libc-dev python3-dev

COPY initdb.sql /docker-entrypoint-initdb.d/
COPY start.sh /

RUN chmod +x /start.sh

EXPOSE 3306

CMD ["/start.sh"]