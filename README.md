# **Pythsql**
A docker image based on alpine linux with python and mysql installed.

## Basic Usage
### Run a container from Pythsql
To run a container from Pythsql, just run the following command:
```
docker run -d -p 3306:3306 -e MYSQL_DATABASE=mydatabase -e MYSQL_USER=myuser -e MYSQL_PASSWORD=mypassword -e MYSQL_ROOT_PASSWORD=myrootpassword --name container_name registry.gitlab.com/bloomverga/pythsql/pythsql
```
### Use Pythsql with `gitlab-ci.yml`
If you want to use this image in a gitlab job pipeline, you need to specify the `variables` section as in the exemple below:

```
stages:
  - first_stage
  - second_stage

first_stage:
  image: registry.gitlab.com/bloomverga/pythsql/pythsql
  stage: test
  variables:
    MYSQL_DATABASE: $MYSQL_DATABASE
    MYSQL_USER: $MYSQL_USER
    MYSQL_PASSWORD: $MYSQL_PASSWORD
    MYSQL_ROOT_PASSWORD: $MYSQL_ROOT_PASSWORD
  script:
    - some_scripts_using_mysql_or_python
    - ...

second_stage:
  image: another_docker_image
  stage: second_stage
  ...
```
## Build Pythsql image by yourself
To use this project, first clone the repository
```
git clone https://gitlab.com/BloomVerga/pythsql.git
```
or download the folder
```
wget https://gitlab.com/BloomVerga/pythsql/-/archive/main/pythsql-main.zip
unzip pythsql-main.zip
```

### Build and use Pythsql locally
Navigate to the unzipped folder and build a docker image locally named `pythsql`. Then launch the container based on the `pythsql` image.
```
docker build -t local_pythsql .
docker run -d -p 3306:3306 -e MYSQL_DATABASE=mydatabase -e MYSQL_USER=myuser -e MYSQL_PASSWORD=mypassword -e MYSQL_ROOT_PASSWORD=myrootpassword --name container_name local_pythsql
```
### Publish the image to your gitlab registry
Navigate to the unzipped folder and Build the docker image and push it to gitlab registry
```
cd pythsql-main
docker build -t registry.gitlab.com/<your-username>/<your-project>/pythsql .
docker login registry.gitlab.com -u <your-username> -p <your-personal-access-token>
docker push registry.gitlab.com/<your-username>/<your-project>/pythsql
```

Now you can run a container using this image
```
docker run -it --name container_name registry.gitlab.com/<your-username>/<your-project>/pythsql
```

### Test the image you built yourself
To test the Docker image you have built and ensure that it's working properly, you can follow these steps:
1. Run a container using 
- the local built image
```
docker run -it --name container_name local_pythsql
```
- the image published on gitlab registry
```
docker run -it --name container_name registry.gitlab.com/<your-username>/<your-project>/pythsql
```
2. Connect to the MySQL server inside the container as `myuser`:

```
docker exec -it container_name mysql -u myuser -p
```
Enter the password when prompted, which is the value of the ``MYSQL_PASSWORD`` environment variable that you set earlier.

3. Run the following command to check if the ``mydatabase`` database exists
```
SHOW DATABASES; exit;
```
If everything is working properly, you should see ``mydatabase`` listed in the output.

4. Connect to the MySQL server inside the container as `root`:
```
docker exec -it container_name mysql -u root
```
Then run the following command to check if the ``myuser`` user has the required privileges on the ``mydatabase`` database:
```
SHOW GRANTS FOR myuser;
```
If everything is working properly, you should see the following output:
```
GRANT USAGE ON *.* TO 'myuser'@'%'
GRANT ALL PRIVILEGES ON `mydatabase`.* TO 'myuser'@'%'
```
### Use your built image with `gitlab-ci.yml`
If you want to use this image in a gitlab job pipeline, you need to specify the `variables` section as in the exemple below:

```
stages:
  - first_stage
  - second_stage

first_stage:
  image: registry.gitlab.com/<your-username>/<your-project>/pythsql
  stage: test
  variables:
    MYSQL_DATABASE: $MYSQL_DATABASE
    MYSQL_USER: $MYSQL_USER
    MYSQL_PASSWORD: $MYSQL_PASSWORD
    MYSQL_ROOT_PASSWORD: $MYSQL_ROOT_PASSWORD
  script:
    - some_scripts_using_mysql_or_python
    - ...

second_stage:
  image: another_docker_image
  stage: second_stage
  ...
```

